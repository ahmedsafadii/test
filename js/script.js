$(document).ready(function(){
    $(window).scroll(function(){
      var sticky = $('.box-run-fixed'),
          scroll = $(window).scrollTop();

      if (scroll >= 200) sticky.addClass('fixed');
      else sticky.removeClass('fixed');
    });
    

    /*open side*/
    $(".btn-side-menu").click(function(){
        $("body,html").addClass('menu-toggle');
    });
    $(".menu-overlay").click(function(){
        $("body,html").removeClass('menu-toggle');
    });

    $(".js-select").each(function(i,v){
        var that = $(this);
        var placeholder = $(that).attr("data-placeholder");
        $(that).select2({
            placeholder:placeholder,
            minimumResultsForSearch: Infinity,
        });
    });

	 /*upload avatar register*/
     var readURL = function(input) {
         if (input.files && input.files[0]) {
             var reader = new FileReader();

             reader.onload = function (e) {
                 $('.up-img-pic').attr('src', e.target.result);
             }
    
             reader.readAsDataURL(input.files[0]);
         }
     }
     $("#up--file").on('change', function(){
         readURL(this);
     });



    $(document).on('click',".add-new-input",function (){
        let h = `<div class="col-lg-4 col-md-4 col-sm-6 item">
                 <div class="mb-3">
                  <label for="style2ControlInput" class="form-label d-flex justify-content-between align-items-center"><span>STYLE</span> <a href="javascript:;" class="text-danger delete-item font-small">Delete</a></label>
                  <input type="text" class="form-control" id="style2ControlInput" placeholder="Write the STYLE">

                 </div>
                 </div>`;

        $(".inputs-list").append(h);
        feather.replace()
        return false;
    })
    $(document).on('click',".add-new-prompt",function (){
        let h = `<div class="prompt-child-item mb-3 item">
                <label for="prompt3ControlInput" class="form-label d-flex justify-content-between align-items-center"><span>Prompt: TWEET</span>  <a href="javascript:;" class="text-danger delete-item font-small">Delete</a></label>
                <div class="input-group">
                <input type="text" class="form-control" placeholder="Write your first prompt" id="prompt3ControlInput" aria-describedby="button-addon2">
                <button class="btn btn-outline-secondary open-setting" type="button" id="button-addon2"><i data-feather="settings"></i></button>
                </div>
                </div>
                `;

        $(".prompts-list").append(h);
        feather.replace()
        return false;
    })
    $(document).on('click',".delete-item",function (){
        $(this).closest('.item').remove();
        return false;
    })

    $(document).on('click',".open-setting",function (){
        $("body,html").addClass('setting-toggle');
        return false;
    })
    $(document).on('click',".drawer-overlay",function (){
        $("body,html").removeClass('setting-toggle');
        return false;
    })

    

})
